//////*****ALL VARIABLES*****//////

const allCuisines = 'indian,chinese,thai,japanese,korean,middle%20eastern';
const searchUrl =
  'https://api.spoonacular.com/recipes/complexSearch?apiKey=5af4b5c40d53406cb4806a2a02dde1c5&diet=vegan&addRecipeInformation=true';

const recipeResultContainer = document.querySelector('.search-results');
const paginationEl = document.querySelector('.pagination');
let activeUrl = `${searchUrl}&cuisine=${allCuisines}`;
let activeFilteredUrl = `${searchUrl}&cuisine=${allCuisines}`;
let sortCriteria = `&sort=time`;
let topLinks = document.querySelectorAll('.top-links');
let favsLink = document.querySelector('.favs');
let favRecipes = JSON.parse(localStorage.getItem('recipes')) ?? [];
let favRecipesFilterObj;
let sortLinks = document.querySelectorAll('.sort-links');
let filterLinksContainer = document.querySelector('.filter-links-container');
const recipeTypes = [
  'Main Course',
  'Side Dish',
  'Appetizer',
  'Salad',
  'Snack',
  'Soup',
  'Sauce',
];
//Sort Variables
const sortTab = document.querySelector('.sort-tab');
const sortSwitchers = document.querySelectorAll('.switcher-btn');

// MODAL Linked Variables
const modal = document.querySelector('.modal-container');
const overlay = document.querySelector('.overlay');
const isVisible = 'is-visible';

//Pages linked variables
let currentPage = 1;
let totalPages = 1;

//////*****ALL FUNCTIONS*****//////
function sleep(ms) {
  const date = Date.now();
  let currentDate = null;
  do {
    currentDate = Date.now();
  } while (currentDate - date < ms);
}

//to be used on adding or removing to myFavs
const alertWindow = (msg, heartClass) => {
  document.querySelector('.heart-msg').classList.add(heartClass);
  document.querySelector('.alert').classList.add('show');
  document.querySelector('.msg').textContent = msg;
  document.querySelector('.alert').classList.remove('hide');
  document.querySelector('.alert').classList.add('showAlert');
  setTimeout(() => {
    document.querySelector('.alert').classList.remove('show');
    document.querySelector('.alert').classList.add('hide');
  }, 1500);
  setTimeout(() => {
    document.querySelector('.heart-msg').classList.remove(heartClass);
  }, 3000);
};

const renderSpinner = function (parentEl) {
  const markUp = `
  <div class="loading"></div>
  `;
  parentEl.innerHTML = '';
  parentEl.insertAdjacentHTML('afterbegin', markUp);
};

const toggleNav = function ({ target }) {
  const expanded = target.getAttribute('aria-expanded') === 'true' || false;
  navButton.setAttribute('aria-expanded', !expanded);
};

const capitalize = s => s.charAt(0).toUpperCase() + s.slice(1);

//Sorts the favRecipes object to match the active sort criteria
const sortFavRecipes = () => {
  sortCriteria === `&sort=time`
    ? favRecipes.sort((a, b) => {
        return b.readyInMinutes - a.readyInMinutes;
      })
    : favRecipes.sort((a, b) => {
        return b.calories - a.calories;
      });
};
//This return the object which includes the counts of all menu types (for filter) from Fav Recipes. Sample Output: {Lunch: 1, Antipasti: 1}
const favRecipesFilterCounts = () => {
  return favRecipes.reduce((prev, cur) => {
    prev[cur.type] = (prev[cur.type] || 0) + 1;
    return prev;
  }, {});
};
//removes active class from any nodelist
const removeActive = function (nodeList) {
  for (node of nodeList) node.classList.remove('active');
};

//When recipe cards are displayed this helps identify if any recipe is in myFavs and highlights it with a pink box shadow through active class
const identifyFavRecipes = () => {
  const recipeCards = document.querySelectorAll('.recipe-card');
  removeActive(recipeCards);
  recipeCards.forEach(el => {
    const recipeId = el.parentElement.parentElement.dataset.open;
    if (favRecipes.some(obj => obj.id === Number(recipeId)))
      el.classList.add('active');
  });
};

//saving favRecipes to local storage
const setLocalStorage = () => {
  localStorage.setItem('recipes', JSON.stringify(favRecipes));
};

//refreshing myFavs data and link
const refreshMyFavs = function () {
  sortFavRecipes();
  favRecipesFilterObj = favRecipesFilterCounts();
  favsLink.innerHTML = favRecipes.length
    ? `myFavs<span>(${favRecipes.length})</span>`
    : `myFavs`;
};
//this is the event that is triggered when the heart is clicked inside recipe modal
const addToMyFavs = function (recipe, bookmark) {
  if (!bookmark.classList.contains('active')) {
    bookmark.classList.add('active');
    favRecipes.push(recipe);
    alertWindow('Added to myFavs!', 'added');
  } else {
    bookmark.classList.remove('active');
    const index = favRecipes.findIndex(el => el.id === recipe.id);
    favRecipes.splice(index, 1);
    alertWindow('Removed from myFavs!', 'removed');
  }
  setLocalStorage();
  refreshMyFavs();
  if (favsLink.classList.contains('active')) displayMyFavsClickResults();
  identifyFavRecipes();
};

//Opening and Closing Modal Functions
const generateModalHTML = recipe => {
  return `
  <div class="modal-recipe"title="${recipe.title}">
    <i class="fas fa-times"></i>
    <h1 class="modal-recipe-title">
      <span>${
        recipe.title.split(' ').length > 5
          ? recipe.title.split(' ').slice(0, 5).join(' ') + '...  '
          : recipe.title
      }
      </span>
    </h1>
    <div class="left-side">
      <figure class="modal-recipe-fig" title="${recipe.title}">
        <img
          src="${recipe.image}"
          alt="${recipe.title}"
          class="modal-recipe-img"
        />
      </figure>
      <div class="modal-recipe-details">
        <div class="modal-recipe-info">
          <i class="modal-recipe-info-icon fa-solid fa-stopwatch"></i>
          <span class="modal-recipe-info-data">${recipe.readyInMinutes}</span>
          <span class="modal-recipe-info-text">minutes</span>
        </div>
        <div class="modal-recipe-info">
          <i class="modal-recipe-info-icon fa-solid fa-person"></i>
          <span class="modal-recipe-info-data">${recipe.servings}</span>
          <span class="modal-recipe-info-text">servings</span>
        </div>
        <div class="modal-recipe-info">
          <i class="modal-recipe-info-icon fa-solid fa-dumbbell"></i>
          <span class="modal-recipe-info-data">
            ${Math.round(recipe.calories)}
          </span>
          <span class="modal-recipe-info-text">calories</span>
        </div>
        <div class="modal-recipe-info">
          <i class="modal-recipe-info-icon fa-solid fa-weight-scale"></i>
          <span class="modal-recipe-info-data">
          ${Math.round(recipe.weight)}
          </span>
          <span class="modal-recipe-info-text">gms</span>
        </div>
        <button class="bookmark-icon" title="Click to add/remove to your favourite recipes">
          <i class="heart fa-solid fa-heart"></i>
        </button>
      </div>
    </div>
          
    <div class="right-side">
      <h2 class="modal-heading">Summary</h2>
      <p class="modal-recipe-directions-text">
      ${
        recipe.summary.includes('Users who liked')
          ? recipe.summary.split('Users who liked')[0]
          : recipe.summary.includes('Similar recipes')
          ? recipe.summary.split('Similar recipes')[0]
          : recipe.summary.includes('Try')
          ? recipe.summary.split('Try')[0]
          : recipe.summary.split('TryIf you like this recipe')[0]
      }
      </p>
      <p class="modal-recipe-directions-text">
        This recipe was carefully designed and tested by
        <span class="recipe-by">${recipe.publisher}</span>. Please check out
        directions at their website.
      </p>
      <a
        class="btn-small"
        href="${recipe.sourceUrl}"
        target="_blank"
      >
       <span>Directions</span>
       <i class="search-icon fa-solid fa-caret-right"></i>
      </a>
    </div>
   </div>
   `;
};
const closeModal = () => {
  modal.classList.remove(isVisible);
  overlay.classList.remove(isVisible);
  document.querySelector('body').classList.remove('hide-body-scroll');
};

const openModal = () => {
  modal.classList.add(isVisible);
  overlay.classList.add(isVisible);
  document.querySelector('body').classList.add('hide-body-scroll');
};

const buildDisplayModal = async function (id) {
  try {
    const markStart = 'mark_start';
    const mark1 = 'mark1';
    const mark2 = 'mark2';
    const mark3 = 'mark3';
    openModal();
    renderSpinner(modal);
    const res = await fetch(
      `https://api.spoonacular.com/recipes/${id}/information?apiKey=5af4b5c40d53406cb4806a2a02dde1c5&includeNutrition=true`
    );
    const data = await res.json();
    if (!res.ok) throw new Error(`${data.message} (${res.status})`);
    let { recipe } = data;
    recipe = {
      id: data.id,
      title: data.title,
      publisher: data.sourceName ? data.sourceName : 'Not known',
      sourceUrl: data.sourceUrl,
      image: data.image,
      servings: data.servings,
      readyInMinutes: data.readyInMinutes,
      calories: data.nutrition.nutrients[0].amount,
      weight: data.nutrition.weightPerServing.amount,
      summary: data.summary,
      cuisines: data.cuisines,
      type: data.dishTypes[0] ? capitalize(data.dishTypes[0]) : 'Not known',
    };
    let markup = generateModalHTML(recipe);
    modal.innerHTML = '';
    modal.innerHTML = markup;
    const bookmark = document.querySelector('.bookmark-icon');
    //to check if this recipe is already bookmarked
    if (favRecipes.find(el => el.id === recipe.id))
      bookmark.classList.add('active');
    //to handle click on heart/bookmark recipe
    bookmark.addEventListener('click', function () {
      addToMyFavs(recipe, bookmark);
    });

    //closeModal by click on cross/overlay/escape key
    document.querySelector('.fa-times').addEventListener('click', closeModal);
    overlay.addEventListener('click', closeModal);
    document.addEventListener('keydown', function (e) {
      if (e.key === 'Escape') closeModal();
    });
  } catch (err) {
    console.log(`${err}🎆🎆`);
  }
};

//This function builds the HTML for each recipe card
const buildRecipeCard = el => {
  return `
<div class="recipe-display-box" title="${el.title}" data-open="${el.id}">
  <div class="recipe-inner">
    <div class="recipe-card recipe-card-front">
      <figure class="recipe-fig">
        <img
          src="${el.image}"
          alt="food"
          class="recipe-img"
        />
        <h1 class="recipe-title">
          <span>${
            el.title.split(' ').length > 3
              ? el.title.split(' ').slice(0, 3).join(' ') + '...  '
              : el.title
          }
          </span>
        </h1>
      </figure>
    </div>
    <div class="recipe-card recipe-card-back">
      <div class="recipe-content">
        <div class="recipe-header">
          <img
            src="${el.image}"
            alt="food"
            class="recipe-img-back"
          />
          <h2>${
            el.title.split(' ').length > 5
              ? el.title.split(' ').slice(0, 5).join(' ') + '...  '
              : el.title
          }
          </h2>
        </div>
        <div class="recipe-body">
          <p><strong>Cuisine:</strong> ${el.cuisines[0]}</p>
          <p><strong>Time to Cook:</strong> ${el.readyInMinutes} mins</p>
          <p><strong>Servings:</strong> For ${el.servings}</p>
        </div>
      </div>
    </div>
  </div>
</div>
  `;
};

//this function picks the results array from favRecipes or the API and displays after sort and filter too
const loadClickResultsHTML = function (resultsArray) {
  let markup = '';
  resultsArray.forEach(el => {
    markup += buildRecipeCard(el);
  });
  recipeResultContainer.innerHTML = '';
  recipeResultContainer.innerHTML = markup;
  // Adding the Card Flip animation
  let cards = document.querySelectorAll('.recipe-inner');
  for (const card of cards) {
    card.addEventListener('mouseenter', function (e) {
      card.classList.toggle('is-flipped');
    });
    card.addEventListener('mouseleave', function (e) {
      card.classList.toggle('is-flipped');
    });
  }
  //Full Site Modal Click Event being added
  const allRecipeCards = document.querySelectorAll('[data-open]');
  for (const elm of allRecipeCards) {
    elm.addEventListener('click', function () {
      const modalId = this.dataset.open;
      buildDisplayModal(modalId);
    });
  }
  identifyFavRecipes();
};
//this will generate the results on a page number click in and send to load the results
const displayPageClickResults = async function (pageNo) {
  try {
    renderSpinner(recipeResultContainer);
    if (!favsLink.classList.contains('active')) {
      const res = await fetch(
        `${activeFilteredUrl}${sortCriteria}&offset=${(pageNo - 1) * 10}`
      );
      const data = await res.json();
      loadClickResultsHTML(data.results);
    } else {
      loadClickResultsHTML(favRecipes.slice(pageNo * 10 - 10, pageNo * 10));
    }
  } catch (err) {
    console.log(`${err}🎆🎆`);
  }
};

//this will generate the results sort click and send to load the results
const displaySortClickResults = async function (sortBy) {
  sortCriteria = `&sort=${sortBy}`;
  sortFavRecipes();
  try {
    renderSpinner(recipeResultContainer);
    paginationEl.innerHTML = '';
    let results;
    if (!favsLink.classList.contains('active')) {
      const res = await fetch(`${activeFilteredUrl}${sortCriteria}`);
      const data = await res.json();
      results = data.results;
      totalPages = Math.ceil(data.totalResults / 10);
    } else {
      const activeFilter = document.querySelector('.filter-links.active')
        .dataset.filter;
      if (activeFilter === '') {
        results = favRecipes.slice(0, 10);
        totalPages = Math.ceil(favRecipes.length / 10);
      } else {
        let filteredFavs = favRecipes.filter(
          recipe => recipe.type === activeFilter
        );
        results = filteredFavs.slice(0, 10);
        totalPages = Math.ceil(filteredFavs.length / 10);
      }
    }
    loadClickResultsHTML(results);
    displayPagination(totalPages);
  } catch (err) {
    console.log(`${err}🎆🎆`);
  }
};

const displayPagination = function (noOfPages) {
  //first markup is the previous arrow and the first page link with current class
  let markup = `
  <i class="btn-icon fa-solid fa-circle-arrow-left" data-prev></i>
  <a href="#" class="page-link active" data-page="1">1</a>
  `;
  //then we use for loop to add pages from page 2 onwards
  for (i = 2; i <= noOfPages; i++) {
    markup += `
    <a href="#" class="page-link" data-page="${i}">${i}</a>
    `;
  }
  //finally we add the markup for next arrow
  markup += `
  <i class="btn-icon fa-solid fa-circle-arrow-right" data-next></i>
  `;
  paginationEl.innerHTML = markup;
  const allPageNoLinks = document.querySelectorAll('.page-link');
  const prevPageLink = document.querySelector('[data-prev]');
  const nextPageLink = document.querySelector('[data-next]');
  for (const link of allPageNoLinks) {
    link.addEventListener('click', function () {
      if (!link.classList.contains('active')) {
        currentPage = link.textContent;
        displayPageClickResults(currentPage);
        removeActive(allPageNoLinks);
        link.classList.add('active');
      }
    });
  }
  prevPageLink.addEventListener('click', function () {
    if (currentPage > 1) {
      currentPage--;
      displayPageClickResults(currentPage);
      removeActive(allPageNoLinks);
      document
        .querySelector(`[data-page='${currentPage}']`)
        .classList.add('active');
    }
  });
  nextPageLink.addEventListener('click', function () {
    if (currentPage < totalPages) {
      currentPage++;
      displayPageClickResults(currentPage);
      removeActive(allPageNoLinks);
      document
        .querySelector(`[data-page='${currentPage}']`)
        .classList.add('active');
    }
  });
};

const displayFilterClickResults = async function (type) {
  try {
    renderSpinner(recipeResultContainer);
    activeFilteredUrl = `${activeUrl}&type=${type.replaceAll(' ', '%20')}`;
    const res = await fetch(`${activeFilteredUrl}${sortCriteria}`);
    const data = await res.json();
    paginationEl.innerHTML = '';
    loadClickResultsHTML(data.results);
    totalPages = Math.ceil(data.totalResults / 10);
    displayPagination(totalPages);
  } catch (err) {
    console.log(`${err}🎆🎆`);
  }
};

//Function to get only the total results value of the Top Links (Cuisines).
const getTopLinkCounts = async function (paramValue, el) {
  try {
    const res = await fetch(`${searchUrl}&cuisine=${paramValue}`);
    const data = await res.json();
    const count = data.totalResults;
    el.textContent = `${el.dataset.name} (${count})`;
  } catch {
    console.log(`${err}🎆🎆`);
  }
};

//function for getting count of each filter (meal type) - this is used in for loop to go over all filters
const getFilterCount = async function (typeValue) {
  try {
    const res = await fetch(
      `${activeUrl}&type=${typeValue.replaceAll(' ', '%20')}`
    );
    const data = await res.json();
    if (data.totalResults) {
      const markup = `
      <li class="filter-links" data-filter="${typeValue}" >
          <a  href="#">${typeValue} <span> (${data.totalResults})</span></a>
        </li>
      `;
      filterLinksContainer.insertAdjacentHTML('beforeend', markup);
      document
        .querySelector(`[data-filter="${typeValue}"]`)
        .addEventListener('click', function () {
          if (!this.classList.contains('active')) {
            displayFilterClickResults(this.dataset.filter);
            const filterLinks = document.querySelectorAll('.filter-links');
            removeActive(filterLinks);
            this.classList.add('active');
          }
        });
    }
  } catch (err) {
    console.log(`${err}🎆🎆`);
  }
};

// This is for generating filter counts for myFavs link
const getFavsFilterCount = function (type, count) {
  const markup = `
  <li class="filter-links" data-filter="${type}" >
      <a  href="#">${type} (${count})</a>
    </li>
  `;
  filterLinksContainer.insertAdjacentHTML('beforeend', markup);
  document
    .querySelector(`[data-filter="${type}"]`)
    .addEventListener('click', function () {
      if (!this.classList.contains('active')) {
        renderSpinner(recipeResultContainer);
        paginationEl.innerHTML = '';
        let filteredFavs = favRecipes.filter(recipe => recipe.type === type);
        loadClickResultsHTML(filteredFavs.slice(0, 10));
        totalPages = Math.ceil(filteredFavs / 10);
        displayPagination(totalPages);
        const filterLinks = document.querySelectorAll('.filter-links');
        removeActive(filterLinks);
        this.classList.add('active');
      }
    });
};

//function to get counts of all filters under an active top link - for myFavs and cuisines
const refreshFilters = function () {
  filterLinksContainer.innerHTML = `
  <li data-filter="" class="active filter-links">
          <a href="#">All</a>
        </li>
  `;
  const allFilter = document.querySelector(`[data-filter=""]`);
  if (!favsLink.classList.contains('active')) {
    allFilter.addEventListener('click', function () {
      if (!this.classList.contains('active')) {
        displayTopClickResults(
          document.querySelector('.top-links.active').dataset.value
        );
      }
    });
    for (const type of recipeTypes) {
      getFilterCount(type);
      sleep(200);
    }
  } else {
    allFilter.addEventListener('click', function () {
      if (!this.classList.contains('active')) {
        displayMyFavsClickResults();
      }
    });
    for (const [type, count] of Object.entries(favRecipesFilterObj)) {
      getFavsFilterCount(type, count);
    }
  }
};

//this is used to refresh and load the myFavs link
const displayMyFavsClickResults = function () {
  renderSpinner(recipeResultContainer);
  refreshFilters();
  paginationEl.innerHTML = '';
  const first10Results = favRecipes.slice(0, 10);
  loadClickResultsHTML(first10Results);
  totalPages = Math.ceil(favRecipes.length / 10);
  displayPagination(totalPages);
};

//this function is run when a top link is clicked. Added in click event listener
const displayTopClickResults = async function (cuisine) {
  try {
    renderSpinner(recipeResultContainer);
    activeUrl = `${searchUrl}&cuisine=${cuisine}`;
    activeFilteredUrl = `${searchUrl}&cuisine=${cuisine}`;
    const allRes = await fetch(`${activeUrl}${sortCriteria}`);
    const allData = await allRes.json();
    paginationEl.innerHTML = '';
    refreshFilters();
    loadClickResultsHTML(allData.results);
    totalPages = Math.ceil(allData.totalResults / 10);
    displayPagination(totalPages);
  } catch (err) {
    console.log(`${err}🎆🎆`);
  }
};

//resetting local storage
const resetLocalStorage = () => {
  localStorage.removeItem('recipes');
  refreshMyFavs();
  displayTopClickResults(allCuisines);
};

//////*****THE WEBSITE LAUNCH/FIRST LOAD JAVASCRIPT*****//////

//Adding event listener to logo
document.querySelector('.logo').addEventListener('click', function () {
  displayTopClickResults(allCuisines);
});

//Adding event listeners to top links(cuisines) as well as getting their total counts
for (const link of topLinks) {
  getTopLinkCounts(link.dataset.value, link); //loading the top links counts
  sleep(200);
  link.addEventListener('click', function () {
    if (!link.classList.contains('active')) {
      displayTopClickResults(link.dataset.value);
      removeActive(topLinks);
      favsLink.classList.remove('active');
      link.classList.add('active');
    }
  });
}

//click event on myFavs link to load bookmarked recipes
favsLink.addEventListener('click', function () {
  if (!this.classList.contains('active')) {
    this.classList.add('active');
    removeActive(topLinks);
    displayMyFavsClickResults();
  }
});

//Adding event listener to sort buttons
for (const elm of sortSwitchers) {
  elm.addEventListener('click', function () {
    if (!elm.classList.contains('active')) {
      displaySortClickResults(elm.dataset.value);
      removeActive(sortSwitchers);
      elm.classList.add('active');
    }
  });
}

//Adding event listener to the sort tab for it to open and close
sortTab.addEventListener('click', function () {
  const tab = this.parentElement.parentElement;
  if (!tab.className.includes('open')) {
    tab.classList.add('open');
  } else {
    tab.classList.remove('open');
  }
});

// Activating Navbar Toggler for smaller screen sizes
const navButton = document.querySelector('.navbar-toggler[aria-expanded]');
navButton.addEventListener('click', toggleNav);

//to display first set of recipes for All Cuisines and refreshing favRecipes from local Storage
refreshMyFavs();
displayTopClickResults(allCuisines);
